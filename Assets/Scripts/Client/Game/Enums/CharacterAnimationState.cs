﻿namespace Karen90MmoFramework.Client.Game
{
	public enum CharacterAnimationState
	{
		Idle				= 0,
		StrafingLeft		= 1,
		StrafingRight		= 2,
		Backing				= 3,
		Running				= 4,
	}
}
