﻿namespace Karen90MmoFramework.Client.Game
{
	public enum UserLoginWindowType
	{
		Splash = 0,
		Login = 1,
		NewUser = 2,
	};
}