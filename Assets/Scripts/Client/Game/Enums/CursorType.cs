﻿namespace Karen90MmoFramework.Client.Game
{
	public enum CursorType : byte
	{
		CURSOR_DEFAULT				= 0,
		CURSOR_TALK					= 1,
		CURSOR_USE					= 2,
	};
}
