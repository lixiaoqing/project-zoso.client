﻿using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Data;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class Spell
	{
		#region Constants and Fields

		/// <summary>
		/// id
		/// </summary>
		private readonly short id;

		/// <summary>
		/// the spell data
		/// </summary>
		private readonly SpellData spellData;

		/// <summary>
		/// player
		/// </summary>
		private Player player;

		/// <summary>
		/// spell manager
		/// </summary>
		private ISpellController spellManager;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the Id
		/// </summary>
		public short Id
		{
			get
			{
				return this.id;
			}
		}

		/// <summary>
		/// Gets the spell data
		/// </summary>
		public SpellData SpellData
		{
			get
			{
				return this.spellData;
			}
		}

		/// <summary>
		/// Gets the name
		/// </summary>
		public string Name { get; private set; }
		
		/// <summary>
		/// Gets the value indicating the spell's affection method
		/// </summary>
		public SpellTargetSelectionMethods TargetSelectionMethod { get; private set; }
		
		/// <summary>
		/// [Flags] Gets the spell school
		/// </summary>
		public SpellSchools School { get; private set; }

		/// <summary>
		/// [Flags] Gets the required weapon types
		/// </summary>
		public WeaponTypes RequiredWeaponType { get; private set; }

		/// <summary>
		/// [Flags] Gets the required target types
		/// </summary>
		public SpellTargetTypes RequiredTargetType { get; private set; }

		/// <summary>
		/// Gets the power type
		/// </summary>
		public Vitals PowerType { get; private set; }

		/// <summary>
		/// Gets the power cost
		/// </summary>
		public int PowerCost { get; private set; }

		/// <summary>
		/// Tells whether the spell is a proc or not
		/// </summary>
		public bool IsProc { get; private set; }

		/// <summary>
		/// Tells whether the spell is affected by the Gcd or not
		/// </summary>
		public bool AffectedByGCD { get; private set; }

		/// <summary>
		/// Tells whether the spell triggers Gcd or not
		/// </summary>
		public bool TriggersGCD { get; private set; }

		/// <summary>
		/// Tells whether the spell is initialized or not
		/// </summary>
		public bool IsReady { get; private set; }

		/// <summary>
		/// Gets the spell state
		/// </summary>
		public SpellStates State { get; private set; }

		public int MinCastRadius { get; private set; }
		public int MaxCastRadius { get; private set; }

		public float CastTime { get; private set; }
		public float Cooldown { get; private set; }

		public float CooldownTimer { get; private set; }

		#endregion

		#region Constructors and Destructors

		public Spell(SpellData spellData)
		{
			this.id = spellData.SpellId;
			this.Name = spellData.Name;
			this.School = spellData.School;
			this.TargetSelectionMethod = spellData.TargetSelectionMethod;
			this.RequiredTargetType = spellData.RequiredTargetType;
			this.RequiredWeaponType = spellData.RequiredWeaponType;
			this.PowerType = spellData.PowerType;
			this.PowerCost = spellData.PowerCost;

			this.MinCastRadius = spellData.MinCastRadius;
			this.MaxCastRadius = spellData.MaxCastRadius;
			this.CastTime = spellData.CastTime;
			this.Cooldown = spellData.Cooldown;

			this.IsProc = spellData.IsProc;
			this.AffectedByGCD = spellData.AffectedByGCD;
			this.TriggersGCD = spellData.TriggersGCD;

			this.spellData = spellData;

			this.State = SpellStates.Null;
			this.IsReady = false;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Initializes the spell by assigning an owner. This MUST be called before the spell can be casted.
		/// </summary>
		public void Initialize(Player player, ISpellController controller)
		{
			this.player = player;
			this.spellManager = controller;
			this.State = this.IsProc ? SpellStates.WaitingForProc : SpellStates.Idle;
			this.IsReady = true;
		}

		/// <summary>
		/// Checks to see whether the spell can be casted or not.
		/// </summary>
		public CastResults CheckSpell()
		{
			if (State != SpellStates.Idle)
				return CastResults.SpellNotReady;

			if (TargetSelectionMethod == SpellTargetSelectionMethods.AreaOfEffect || TargetSelectionMethod == SpellTargetSelectionMethods.Cone)
				// needs to check whether the character is in live or ghost mode
				if (player.HaveVital(this.PowerType, this.PowerCost))
					return CastResults.Ok;
				else
					return CastResults.NotEnoughPower;

			if ((RequiredTargetType & SpellTargetTypes.Self) != SpellTargetTypes.Self)
				return CastResults.TargetRequired;

			return CastResults.Ok;
		}

		/// <summary>
		///  Checks to see whether the spell can be casted on a character or not.
		/// </summary>
		public CastResults CheckSpell(MmoObject target)
		{
			if (target == null || target == this.player)
				return this.CheckSpell();

			if (State != SpellStates.Idle)
				return CastResults.SpellNotReady;

			if (TargetSelectionMethod == SpellTargetSelectionMethods.AreaOfEffect || TargetSelectionMethod == SpellTargetSelectionMethods.Cone)
				// needs to check whether the character is in live or ghost mode
				if (player.HaveVital(this.PowerType, this.PowerCost))
					return CastResults.Ok;
				else
					return CastResults.NotEnoughPower;

			switch ((ObjectType)target.Guid.Type)
			{
				case ObjectType.Player:
					{
						// for now all the players are friendly

						var actor = target as OtherPlayer;
						if ((!actor.IsDead && (RequiredTargetType & SpellTargetTypes.FriendlyUnit) == SpellTargetTypes.FriendlyUnit) == false &&
							(actor.IsDead && (RequiredTargetType & SpellTargetTypes.FriendlyCorpse) == SpellTargetTypes.FriendlyCorpse) == false)
							return CastResults.InvalidTarget;

					}
					break;

				case ObjectType.Npc:
					{
						var npc = (Npc) target;
						if (npc.Alignment == SocialAlignment.Evil || npc.Alignment == SocialAlignment.NeutralEvil)
						{
							if ((!npc.IsDead && (RequiredTargetType & SpellTargetTypes.HostileUnit) == SpellTargetTypes.HostileUnit) == false &&
								(npc.IsDead && (RequiredTargetType & SpellTargetTypes.HostileCorpse) == SpellTargetTypes.HostileCorpse) == false)
								return CastResults.InvalidTarget;
						}
						else if (npc.Alignment == SocialAlignment.Good || npc.Alignment == SocialAlignment.NeutralGood)
						{
							if ((!npc.IsDead && (RequiredTargetType & SpellTargetTypes.FriendlyUnit) == SpellTargetTypes.FriendlyUnit) == false &&
								(npc.IsDead && (RequiredTargetType & SpellTargetTypes.FriendlyCorpse) == SpellTargetTypes.FriendlyCorpse) == false)
								return CastResults.InvalidTarget;
						}
					}
					break;

				case ObjectType.Gameobject:
					throw new System.NotImplementedException();
			}

			if (!player.HaveVital(this.PowerType, this.PowerCost))
				return CastResults.NotEnoughPower;

			var distance = player.Position.GetDistance(target.Position);
			if (distance < this.MinCastRadius)
				return CastResults.TargetTooClose;
			return distance > this.MaxCastRadius ? CastResults.OutOfRange : CastResults.Ok;
		}

		/// <summary>
		/// Updates the spell
		/// </summary>
		/// <param name="deltaTime"></param>
		public void Update(float deltaTime)
		{
			this.CooldownTimer += deltaTime;
			if (CooldownTimer >= Cooldown)
			{
				this.EndCooldown();
			}
		}
		
		/// <summary>
		/// Call this when the <see cref="Spell"/> begins its cooldown
		/// </summary>
		public void BeginCooldown()
		{
			this.State = SpellStates.Cooldown;
			this.CooldownTimer = 0;

			this.spellManager.AddSpellUpdateEvent(this);
		}

		/// <summary>
		/// Call this when the <see cref="Spell"/> ends its cooldown
		/// </summary>
		public void EndCooldown()
		{
			this.spellManager.RemoveSpellUpdateEvent(this);
			this.State = this.IsProc ? SpellStates.WaitingForProc : SpellStates.Idle;
		}

		#endregion
	}
}
