﻿namespace Karen90MmoFramework.Client.Game.Systems
{
	public interface ISpellController
	{
		/// <summary>
		/// Adds a spell update event
		/// </summary>
		/// <param name="spell"></param>
		void AddSpellUpdateEvent(Spell spell);

		/// <summary>
		/// Removes a spell update event
		/// </summary>
		/// <param name="spell"></param>
		void RemoveSpellUpdateEvent(Spell spell);
	}
}
