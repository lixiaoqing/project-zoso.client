﻿using UnityEngine;
using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Game.Objects
{
	public interface IAvatar
	{
		/// <summary>
		/// Gets or sets the avatar's name
		/// </summary>
		string AvatarName { get; set; }

		/// <summary>
		/// Avatar's Position
		/// </summary>
		Vector3 Position { get; }

		/// <summary>
		/// Avatar's Rotation
		/// </summary>
		Vector3 Rotation { get; }

		/// <summary>
		/// Gets the center
		/// </summary>
		Vector3 Center { get; }

		/// <summary>
		/// Gets the height
		/// </summary>
		float Height { get; }

		/// <summary>
		/// Spawns the Avatar
		/// </summary>
		void Spawn(Vector3 position);

		/// <summary>
		/// Destroys the Avatar
		/// </summary>
		void Destroy();

		/// <summary>
		/// Sets an Owner
		/// </summary>
		void SetOwner(MmoObject owner);

		/// <summary>
		/// Sets the Position
		/// </summary>
		void SetPosition(Vector3 newPosition);

		/// <summary>
		/// Sets the rotation
		/// </summary>
		void SetRotation(Vector3 newRotation);

		/// <summary>
		/// Sets the minimap blip icon
		/// </summary>
		/// <param name="iconType"></param>
		void SetBlipIcon(BlipIconType iconType);

		/// <summary>
		/// Sets the overhead icon
		/// </summary>
		/// <param name="iconType"></param>
		void SetOverheadIcon(OverheadIconType iconType);

		/// <summary>
		/// Allows interaction to this object.
		/// </summary>
		void AllowInteraction();

		/// <summary>
		/// Denies interaction to this object.
		/// </summary>
		void DenyInteraction();

		/// <summary>
		/// Sets the movement state
		/// </summary>
		void SetState(MovementState state);

		/// <summary>
		/// Sets the movement direction
		/// </summary>
		void SetDirection(MovementDirection direction);

		/// <summary>
		/// Creates a loot indicator
		/// </summary>
		void CreateLootIndicator();

		/// <summary>
		/// Clears loot indicator
		/// </summary>
		void ClearLootIndicator();

		/// <summary>
		/// Handles what to do when the player focuses on this <see cref="IAvatar"/>.
		/// </summary>
		void HandlePlayerFocus();

		/// <summary>
		/// Handles what to do when the player loses focuses from this <see cref="IAvatar"/>.
		/// </summary>
		void HandlePlayerUnfocus();
	}
}
