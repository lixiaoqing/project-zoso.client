﻿namespace Karen90MmoFramework.Client.Game
{
	public static class GameSettings
	{
		// HOT KEYS
		public const int LEFT_MOUSE_BUTTON = 0;
		public const int RIGHT_MOUSE_BUTTON = 1;

		// Interactable
		public const float MIN_INTERACTION_RANGE = 5.0f;
		public const float MIN_INSTANT_SNAP_DISTANCE = 4f;
		public const float MIN_IGNORE_LERP_DISTANCE = 0.3f;
		public const float MIN_INSTANT_ROTATION_ANGLE = 120f;
		public const float PLAYER_SMOOTH_ORIENTATION_FACTOR = 15;
		public const float NPC_SMOOTH_ORIENTATION_FACTOR = 8;

		// Player Consts
		public const float PLAYER_TARGET_MIN_DISTANCE = 45.0f;
		public const float PLAYER_SPEED_CHECK_TIME = 0.1f;
		public const int MAX_PLAYER_LEVEL = 40;

		// NPC Consts
		public const int NPC_PRIMARY_ACTION_KEY = RIGHT_MOUSE_BUTTON;
		public const int NPC_SECONDARY_ACTION_KEY = LEFT_MOUSE_BUTTON;

		// Item Consts
		public const int ITEM_ICON_SIZE = 40;
		public const int SPELL_ICON_SIZE = 48;

		// Merchant Gui Consts
		public const int GUI_SHOP_SLOT_PER_COLUMN = 2;
		public const int GUI_SHOP_SLOT_PER_ROW = 5;

		// Chat Consts
		public const int MAX_CHAT_STRINGS = 50;
		public const int MAX_CHAT_LETTERS = 255;
		public const float CHAT_FADE_DELAY = 30.0f; // 30s
		
		// Containers
		public const int MAX_INVENTORY_CONTAINER_SPACE = 80;
		public const int ACTION_BAR_SIZE = 10;

		// Quest
		public const int MAX_ACTIVE_QUESTS = 10;

		// Spells
		public const short USE_SPELL_ID = 0;

		public const string IDLE_ANIMATION_NAME = "idle";
		public const string WALK_ANIMATION_NAME = "walk";
		public const string RUN_ANIMATION_NAME = "run";
		public const string STRAFE_LEFT_ANIMATION_NAME = "strafeLeft";
		public const string STRAFE_RIGHT_ANIMATION_NAME = "strafeRight";
		public const string BACK_ANIMATION_NAME = "walk";
	}; 
}
