﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client;
using Karen90MmoFramework.Client.Game;
using Karen90MmoFramework.Client.Game.Objects;
using Karen90MmoFramework.Client.Data;

namespace Karen90MmoFramework.Hud
{
	public sealed class TooltipManager : IGameHandler
	{
		#region Constants and Fields

		private class ButtonItem
		{
			/// <summary>
			/// Button Name
			/// </summary>
			public string Name { get; set; }

			/// <summary>
			/// Button Description
			/// </summary>
			public string Description { get; set; }

			public TooltipStyle TooltipType
			{
				get
				{
					return TooltipStyle.Button;
				}
			}

			public void Draw(Rect rect, GUISettings guiStyles, GUILayoutOption layoutOption)
			{
				GUI.Box(rect, this.Name, guiStyles.GUIStyleWindowTooltip);
				GUI.Label(new Rect(rect.x + 7, rect.y + 20, 140, 40), this.Description, guiStyles.GUIStyleTooltipInfo);
			}
		}

		/// <summary>
		/// instance
		/// </summary>
		private static TooltipManager _instance;

		/// <summary>
		/// gui settings
		/// </summary>
		private readonly GUISettings guiSettings;

		/// <summary>
		/// item cache
		/// </summary>
		private readonly MmoItemDataStorage itemCache;

		/// <summary>
		/// the game
		/// </summary>
		private GameHandler game;

		private TooltipStyle tooltipType;
		private MmoItemData mmoItem;
		private SpellData spell;
		private Character character;
		private Gameobject go;
		private ButtonItem button;

		private static bool ptt_val;
		private static string ptt_string;
		private static Rect ptt_rect;
		private static bool ptt_can_offset_rect;
		private static bool ptt_is_updating;

		private bool showTooltip;
		private bool halt;

		private Rect rectTooltip;

		private readonly int tooltipOffsetX;
		private readonly int tooltipOffsetY;

		private readonly int floatingTooltipWidth;

		private readonly int staticTooltipWidth;
		private readonly int staticTooltipHeight;

		readonly List<Rect> tRects;
		readonly List<string> tStrings;

		bool doPreDraw;

		#endregion

		#region Properties

		/// <summary>
		/// The Instance
		/// </summary>
		public static TooltipManager Instance
		{
			get
			{
				return _instance ?? (_instance = new TooltipManager());
			}
		}

		/// <summary>
		/// Gets or sets the current tooltip control
		/// </summary>
		public static int tooltipControl { get; set; }

		#endregion

		#region Constructors and Destructors

		private TooltipManager()
		{
			this.tRects = new List<Rect>();
			this.tStrings = new List<string>();

			this.guiSettings = GameManager.Instance.GuiSettings;
			this.itemCache = MmoItemDataStorage.Instance;

			this.button = new ButtonItem();

			this.tooltipOffsetX = 8;
			this.tooltipOffsetY = 16;

			this.floatingTooltipWidth = 220;

			this.staticTooltipWidth = 160;
			this.staticTooltipHeight = 60;
		}

		#endregion

		#region IGameHandler Implementation

		/// <summary>
		/// Initializes the handler
		/// </summary>
		void IGameHandler.Initialize()
		{
			this.showTooltip = false;

			this.rectTooltip = new Rect();
			
			this.tRects.Clear();
			this.tStrings.Clear();
		}

		/// <summary>
		/// Updates the handler
		/// </summary>
		void IGameHandler.Update()
		{
			ptt_is_updating = false;
		}

		/// <summary>
		/// Performs drawing the GUI
		/// </summary>
		void IGameHandler.Draw()
		{
			if (ptt_val)
			{
				ptt_val = false;

				this.showTooltip = this.ProcessTooltip(ptt_string);
				this.OnPreDraw();

				if (tooltipControl == 0)
					tooltipControl = -1;
			}

			if (Event.current.type == EventType.Repaint && showTooltip && !halt)
			{
				this.DrawTooltip();
			}
		}

		/// <summary>
		/// Called after all updates have finished
		/// </summary>
		void IGameHandler.LateUpdate()
		{
			if (showTooltip && (tooltipControl == 0 || !ptt_is_updating))
			{
				this.OnPostDraw();

				tooltipControl = 0;
				showTooltip = false;
			}
		}

		/// <summary>
		/// Destroys the handler
		/// </summary>
		void IGameHandler.Destroy(DestroyReason destroyReason)
		{
			this.mmoItem = null;
			this.spell = null;
			this.button = null;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Sets up the chat manager
		/// </summary>
		/// <param name="gameHandler"></param>
		public void Setup(GameHandler gameHandler)
		{
			this.game = gameHandler;
		}

		/// <summary>
		/// Halts the tooltip from showing temporarily
		/// </summary>
		public void Halt()
		{
			this.halt = true;
		}

		/// <summary>
		/// Releases the Halt Call
		/// </summary>
		public void Release()
		{
			this.halt = false;
		}

		/// <summary>
		/// Sets a tooltip
		/// </summary>
		public static void SetTooltip(Rect rect, string tooltip)
		{
			if (Instance != null)
			{
				ptt_val = true;
				ptt_rect = rect;
				ptt_string = tooltip;
				ptt_can_offset_rect = true;
			}
		}

		/// <summary>
		/// Sets a tooltip
		/// </summary>
		public static void SetTooltip(Rect rect, Vector2 offset, string tooltip)
		{
			if (Instance != null)
			{
				ptt_val = true;
				ptt_rect = new Rect(rect.x + offset.x, rect.y + offset.y, rect.width, rect.height);
				ptt_string = tooltip;
				ptt_can_offset_rect = true;
			}
		}

		/// <summary>
		/// Sets a tooltip
		/// </summary>
		public static void SetTooltip(string tooltip)
		{
			if (Instance != null)
			{
				ptt_val = true;
				ptt_string = tooltip;
				ptt_can_offset_rect = false;
			}
		}

		/// <summary>
		/// Closes the custom tooltip
		/// </summary>
		public static void CloseTooltip()
		{
			if(Instance != null)
				tooltipControl = 0;
		}

		/// <summary>
		/// This method has to be called every frame in order for the tooltip to appear
		/// </summary>
		public static void ValidateTooltip()
		{
			ptt_is_updating = true;
		}
		
		#endregion

		#region Local Methods

		bool ProcessTooltip(string tooltip)
		{
			string[] tooltipData = tooltip.Split('#');
			this.tooltipType = (TooltipStyle)Enum.Parse(typeof(TooltipStyle), tooltipData[0]);

			switch (tooltipType)
			{
				case TooltipStyle.Item:
					{
						short itemId;
						if (short.TryParse(tooltipData[1], out itemId) == false ||
							this.itemCache.TryGetMmoItem(itemId, out this.mmoItem) == false)
						{
							Logger.Error("Item load failed...");
							return false;
						}
					}
					return true;

				case TooltipStyle.Spell:
					{
						short spellId;
						if (short.TryParse(tooltipData[1], out spellId) == false ||
							this.itemCache.TryGetSpell(spellId, out this.spell) == false)
						{
							Logger.Error("Spell load failed...");
							return false;
						}
					}
					return true;

				case TooltipStyle.Character:
					{
						long guid;
						MmoObject mmoObject;

						if (long.TryParse(tooltipData[1], out guid) == false || game.World.TryGetMmoObject(guid, out mmoObject) == false)
						{
							Logger.Error("MmoObject load failed...");
							return false;
						}

						this.character = mmoObject as Character;
					}
					return true;

				case TooltipStyle.GameObject:
					{
						long guid;
						MmoObject mmoObject;

						if (long.TryParse(tooltipData[1], out guid) == false || game.World.TryGetMmoObject(guid, out mmoObject) == false)
						{
							Logger.Error("MmoObject load failed...");
							return false;
						}

						this.go = mmoObject as Gameobject;
					}
					return true;

				case TooltipStyle.Button:
					{
						this.button.Name = tooltipData[1];
						this.button.Description = tooltipData[2];
					}
					break;

				default:
					{
						Logger.DebugFormat("tooltip type {0} not recognized", this.tooltipType);
					}
					return false;
			}

			return false;
		}

		#endregion

		#region GUI Methods

		void DrawTooltip()
		{
			if (doPreDraw)
			{
				this.OnPreDraw();
				this.doPreDraw = false;
			}

			switch (tooltipType)
			{
				case TooltipStyle.Item:
					{
						this.DrawItem();
					}
					break;

				case TooltipStyle.Character:
					{
						this.DrawCharacter();
					}
					break;

				case TooltipStyle.GameObject:
					{
						this.DrawGO();
					}
					break;

				case TooltipStyle.Spell:
					{
						this.DrawSpell();
					}
					break;

				case TooltipStyle.Button:
					{
						this.DrawButton();
					}
					break;
			}
		}

		void OnPreDraw()
		{
			switch (tooltipType)
			{
				case TooltipStyle.Item:
					{
						float x = 10, y = 7;
						float width = this.floatingTooltipWidth - 20;

						var tContent = new GUIContent();

						// 1 name
						var name = mmoItem.Name;
						tContent.text = name;
						var height = this.guiSettings.GUIStyleTooltipItemName.CalcHeight(tContent, width);
						this.guiSettings.GUIStyleTooltipItemName.normal.textColor = GetRarityColor(mmoItem.Rarity);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(name);
						y += height;

						// 2 level
						var levelString = string.Format("Level {0}", mmoItem.Level);
						tContent.text = levelString;
						height = this.guiSettings.GUIStyleTooltipItemInfo.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(levelString);

						// 3 rarity
						var rarityString = mmoItem.Rarity.ToString();
						tContent.text = rarityString;
						var twidth = this.guiSettings.GUIStyleTooltipItemInfo.CalcSize(tContent).x;
						this.tRects.Add(new Rect(width - twidth + x, y, width, height));
						this.tStrings.Add(rarityString);
						y += height;

						// 4 item  type
						var typeString = GetItemTypeString(mmoItem);
						tContent.text = typeString;
						height = this.guiSettings.GUIStyleTooltipItemInfo.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(typeString);
						y += height + 5;
						
						// 5 / ! use description
						if (mmoItem.UseSpellId > 0)
						{
							SpellData itemSpell;
							if (itemCache.TryGetSpell(mmoItem.UseSpellId, out itemSpell))
							{
								var useDescription = itemSpell.Description;
								tContent.text = useDescription;
								height = this.guiSettings.GUIStyleTooltipItemInfoUse.CalcHeight(tContent, width);
								this.tRects.Add(new Rect(x, y, width, height));
								this.tStrings.Add(useDescription);
								y += height + 5;
							}
						}

						// 6 / 5 item description
						var itemDescription = mmoItem.Description;
						tContent.text = itemDescription;
						height = this.guiSettings.GUIStyleTooltipItemDescription.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(itemDescription);
						y += height + 5;

						var tCoord = ptt_can_offset_rect ? new Vector2(ptt_rect.xMax, ptt_rect.y) : Event.current.mousePosition;
						this.rectTooltip = new Rect(tCoord.x + tooltipOffsetX, tCoord.y - tooltipOffsetY, this.floatingTooltipWidth, y);

						if (rectTooltip.xMax > Screen.width)
						{
							if (ptt_can_offset_rect)
								rectTooltip.x -= rectTooltip.xMax - ptt_rect.xMax - tooltipOffsetX;
							else
								rectTooltip.x -= rectTooltip.xMax - Screen.width;
						}
						else if (rectTooltip.x < 0)
						{
							if (ptt_can_offset_rect)
								rectTooltip.x = ptt_rect.xMax + tooltipOffsetX;
							else
								rectTooltip.x += 1 - rectTooltip.x;
						}

						if (rectTooltip.yMax > Screen.height)
						{
							rectTooltip.y -= rectTooltip.yMax - Screen.height;
						}
						else if (rectTooltip.y < 0)
						{
							rectTooltip.y += 1 - rectTooltip.y;
						}
					}
					break;

				case TooltipStyle.Spell:
					{
						float x = 10, y = 7;
						float width = this.floatingTooltipWidth - 20;

						var tContent = new GUIContent();

						// 1 name
						var name = spell.Name;
						tContent.text = name;
						var height = this.guiSettings.GUIStyleTooltipItemName.CalcHeight(tContent, width);
						this.guiSettings.GUIStyleTooltipItemName.normal.textColor = Color.yellow;
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(name);

						// 2 rank
						var rankString = string.Format("Rank {0}", 1);
						tContent.text = rankString;
						var tsize = this.guiSettings.GUIStyleTooltipItemInfo.CalcSize(tContent);
						this.tRects.Add(new Rect(width - tsize.x + x, (y + tsize.y) / 2, width, height));
						this.tStrings.Add(rankString);
						y += height;

						// 3 mana cost
						var manaString = string.Format("Costs {0} {1}", spell.PowerCost, spell.PowerType);
						tContent.text = manaString;
						height = this.guiSettings.GUIStyleTooltipItemInfo.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(manaString);

						// 4 range
						var rangeString = string.Format("{0} - {1} m Range", spell.MinCastRadius, spell.MaxCastRadius);
						tContent.text = rangeString;
						var twidth = this.guiSettings.GUIStyleTooltipItemInfo.CalcSize(tContent).x;
						this.tRects.Add(new Rect(width - twidth + x, y, width, height));
						this.tStrings.Add(rangeString);
						y += height;

						// 5 cast time
						var castString = string.Format("Cast time: {0} s", spell.CastTime);
						tContent.text = castString;
						height = this.guiSettings.GUIStyleTooltipItemInfo.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(castString);

						// 6 cooldown
						var cooldownString = string.Format("Cooldown: {0} s", spell.CastTime);
						tContent.text = cooldownString;
						twidth = this.guiSettings.GUIStyleTooltipItemInfo.CalcSize(tContent).x;
						this.tRects.Add(new Rect(width - twidth + x, y, width, height));
						this.tStrings.Add(cooldownString);
						y += height + 5;

						// 7 description
						var description = spell.Description;
						tContent.text = description;
						height = this.guiSettings.GUIStyleTooltipItemInfoUse.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(description);
						y += height + 5;

						var tCoord = ptt_can_offset_rect ? new Vector2(ptt_rect.xMax, ptt_rect.y) : Event.current.mousePosition;
						this.rectTooltip = new Rect(tCoord.x + tooltipOffsetX, tCoord.y - tooltipOffsetY, this.floatingTooltipWidth, y);

						if (rectTooltip.xMax > Screen.width)
						{
							if (ptt_can_offset_rect)
								rectTooltip.x -= rectTooltip.xMax - ptt_rect.xMax - tooltipOffsetX;
							else
								rectTooltip.x -= rectTooltip.xMax - Screen.width;
						}
						else if (rectTooltip.x < 0)
						{
							if (ptt_can_offset_rect)
								rectTooltip.x = ptt_rect.xMax + tooltipOffsetX;
							else
								rectTooltip.x += 1 - rectTooltip.x;
						}

						if (rectTooltip.yMax > Screen.height)
						{
							rectTooltip.y -= rectTooltip.yMax - Screen.height;
						}
						else if (rectTooltip.y < 0)
						{
							rectTooltip.y += 1 - rectTooltip.y;
						}
					}
					break;

				case TooltipStyle.Character:
					{
						float x = 10, y = 7;
						float width = staticTooltipWidth - 20;

						var tContent = new GUIContent();

						var nameString = string.Format("{0} [{1}]", character.Name, character.Level);
						tContent.text = nameString;
						var height = this.guiSettings.GUIStyleTooltipNpcName.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(nameString);
						y += height - 2;

						var typeString = GetCharTypeString(character);
						tContent.text = typeString;
						height = this.guiSettings.GUIStyleTooltipNpcHostility.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(typeString);

						this.rectTooltip = new Rect(Screen.width - staticTooltipWidth - 10, Screen.height - staticTooltipHeight - 70, staticTooltipWidth, staticTooltipHeight);
					}
					break;

				case TooltipStyle.GameObject:
					{
						float x = 10, y = 7;
						float width = staticTooltipWidth - 20;

						var tContent = new GUIContent();

						var nameString = go.Name;
						tContent.text = nameString;
						var height = this.guiSettings.GUIStyleTooltipNpcName.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(nameString);
						y += height - 2;

						var typeString = GetGOTypeString(go);
						tContent.text = typeString;
						height = this.guiSettings.GUIStyleTooltipNpcHostility.CalcHeight(tContent, width);
						this.tRects.Add(new Rect(x, y, width, height));
						this.tStrings.Add(typeString);

						this.rectTooltip = new Rect(Screen.width - staticTooltipWidth - 10, Screen.height - staticTooltipHeight - 70, staticTooltipWidth, staticTooltipHeight);
					}
					break;
			}
		}

		void DrawItem()
		{
			GUI.BeginGroup(rectTooltip, this.guiSettings.GUIStyleWindowTooltip);
			{
				var index = 0;

				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemName);
				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);
				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);
				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);

				if (mmoItem.UseSpellId > 0)
				{
					GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfoUse);
				}

				GUI.Label(tRects[index], tStrings[index], this.guiSettings.GUIStyleTooltipItemDescription);
			}
			GUI.EndGroup();
		}

		void DrawSpell()
		{
			GUI.BeginGroup(rectTooltip, this.guiSettings.GUIStyleWindowTooltip);
			{
				var index = 0;

				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemName);

				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);
				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);
				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);
				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);
				GUI.Label(tRects[index], tStrings[index++], this.guiSettings.GUIStyleTooltipItemInfo);

				GUI.Label(tRects[index], tStrings[index], this.guiSettings.GUIStyleTooltipItemInfoUse);
			}
			GUI.EndGroup();
		}

		void DrawCharacter()
		{
			GUI.BeginGroup(rectTooltip, this.guiSettings.GUIStyleWindowTooltip);
			{
				GUI.Label(tRects[0], tStrings[0], this.guiSettings.GUIStyleTooltipNpcName);
				GUI.Label(tRects[1], tStrings[1], this.guiSettings.GUIStyleTooltipNpcHostility);
			}
			GUI.EndGroup();
		}

		void DrawGO()
		{
			GUI.BeginGroup(rectTooltip, this.guiSettings.GUIStyleWindowTooltip);
			{
				GUI.Label(tRects[0], tStrings[0], this.guiSettings.GUIStyleTooltipNpcName);
				GUI.Label(tRects[1], tStrings[1], this.guiSettings.GUIStyleTooltipNpcHostility);
			}
			GUI.EndGroup();
		}

		void DrawButton()
		{
			//this.button.Draw(rectTooltip, GuiSettings, null);
		}

		void OnPostDraw()
		{
			this.tRects.Clear();
			this.tStrings.Clear();

			this.mmoItem = null;
			this.spell = null;
			this.character = null;
			this.go = null;
		}

		static string GetItemTypeString(MmoItemData mmoItem)
		{
			switch (mmoItem.ItemType)
			{
				case MmoItemType.Consumable:
					return "Consumable";

				case MmoItemType.Generic:
					return "Generic";

				case MmoItemType.Equipment:
					return "Equipment";

				case MmoItemType.Weapon:
					return "Weapon";
			}

			return "UNKNOWN";
		}

		static string GetGOTypeString(Gameobject go)
		{
			switch (go.GOType)
			{
				case GameObjectType.Chest:
					return "Chest";

				case GameObjectType.Plant:
					return "Plant";

				case GameObjectType.Vein:
					return "Vein";

				case GameObjectType.Item:
					return "Item";
			}

			return "UNKNOWN";
		}

		static string GetCharTypeString(Character character)
		{
			switch((ObjectType)character.Guid.Type)
			{
				case ObjectType.Player:
					return "Player";

				case ObjectType.Npc:
					return character.Species.ToEnumString();
			}
			
			return "UNKNOWN";
		}

		static Color GetRarityColor(Rarity rarity)
		{
			switch (rarity)
			{
				case Rarity.Common:
					return Color.white;

				case Rarity.Uncommon:
					return Color.green;
			}

			return Color.white;
		}

		#endregion
	}
}
