﻿using UnityEngine;

using System.Collections.Generic;

using Karen90MmoFramework.Client.Game;

namespace Karen90MmoFramework.Hud
{
	public partial class HUD
	{
		#region Defines

		struct ScreenMessage
		{
			public readonly NotificationType MessageType;

			public readonly string Title;
			public readonly string Message;
			public readonly float Duration;

			public ScreenMessage(NotificationType messageType, string message, float duration = 2.0f)
			{
				this.MessageType = messageType;
				this.Message = message;
				this.Duration = duration;

				this.Title = this.MessageType.ToString().Replace('_', ' ');
			}
		}

		#endregion

		#region Constants and Fields

		private Queue<ScreenMessage> messageQueue;
		ScreenMessage currNotification;

		private Rect rectTitle;
		private Rect rectMessage;

		private float currentVisibleAmount;
		private float? time;
		private bool lerpAlpha;

		#endregion

		#region Public Methods

		/// <summary>
		/// Queues a screen message to be showed
		/// </summary>
		public void QueueNotification(NotificationType messageType, string message, float duration = 2.0f)
		{
			this.messageQueue.Enqueue(new ScreenMessage(messageType, message, duration));
		}

		#endregion

		#region Local Methods

		void InitializeEventNotifierMembers()
		{
			this.messageQueue = new Queue<ScreenMessage>();
			this.time = null;

			this.currentVisibleAmount = 1.0f;
			this.lerpAlpha = false;

			rectTitle = new Rect
				{
					x = 0,
					y = 120,
					width = Screen.width,
					height = 50
				};

			rectMessage = new Rect
				{
					x = 0,
					y = 160,
					width = Screen.width,
					height = 30
				};
		}

		void UpdateEventNotifier()
		{
			if (lerpAlpha)
			{
				this.currentVisibleAmount = Mathf.Lerp(this.currentVisibleAmount, 0, Time.smoothDeltaTime * 5);

				if (this.currentVisibleAmount <= 0.02f)
				{
					this.lerpAlpha = false;

					time = null;
					this.messageQueue.Dequeue();
				}
			}
		}

		void HandleEventNotifier()
		{
			if (messageQueue.Count > 0)
			{
				if (Event.current.type == EventType.Repaint)
				{
					if (!time.HasValue)
					{
						this.currNotification = messageQueue.Peek();
						this.time = Time.time;
						this.currentVisibleAmount = 1.0f;
					}
					else
					{
						switch (currNotification.MessageType)
						{
							case NotificationType.Quest_Completed:
							case NotificationType.Quest_Started:
								{
									this.ShowQuestMessage(this.currentVisibleAmount);
								}
								break;

							case NotificationType.Quest_Progress:
								{
									this.ShowProgressMessage(this.currentVisibleAmount);
								}
								break;
						}

						if (Time.time - this.time.Value >= this.currNotification.Duration)
						{
							this.lerpAlpha = true;
						}
					}
				}
			}
		}

		// visibleAmount == alpha
		void ShowProgressMessage(float visibleAmount = 1.0f)
		{
			GUIX.LabelOutline(rectMessage, this.currNotification.Message, 1.5f, visibleAmount, guiSettings.GUIStyleScreenProgressMessage);
		}

		// visibleAmount == alpha
		void ShowQuestMessage(float visibleAmount = 1.0f)
		{
			GUIX.LabelOutline(rectTitle, this.currNotification.Title, 2, visibleAmount, guiSettings.GUIStyleScreenMessageTitle);
			GUIX.LabelOutline(rectMessage, this.currNotification.Message, 1, visibleAmount, guiSettings.GUIStyleScreenMessageMessage);
		}

		#endregion
	}
}
