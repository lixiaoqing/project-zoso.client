﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client;
using Karen90MmoFramework.Client.Data;
using Karen90MmoFramework.Client.Game;
using Karen90MmoFramework.Client.Game.Objects;
using Karen90MmoFramework.Client.Game.Systems;

namespace Karen90MmoFramework.Hud
{
	public class WindowManager
	{
		#region Constants and Fields

		struct DraggableSpell : IDraggable
		{
			#region Implementation of IDraggable

			/// <summary>
			/// Gets the item id
			/// </summary>
			public short ItemId { get; set; }

			/// <summary>
			/// Gets the drag texture
			/// </summary>
			public Texture2D Icon { get; set; }

			/// <summary>
			/// Gets the item type
			/// </summary>
			public DragItemType DragItemType
			{
				get
				{
					return DragItemType.Spell;
				}
			}

			#endregion
		}

		private static WindowManager _instance;

		private readonly GUISettings guiSettings;
		private GameHandler game;
		private Player player;

		#region Interaction

		/// <summary>
		/// interaction window
		/// </summary>
		private Action IW_Callback;

		private readonly List<Rect> ILRects;
		private readonly List<GUIContent> ILContents;
		private readonly List<string> ILStrings;

		private InteractionWindowType IWType;

		private readonly Texture2D textureCoin;
		private Texture2D textureInteractionWindow;

		private Rect rectInteractionWindow;
		private Rect rectInteractionWindowTitle;
		private Rect rectInteractionGroup;
		private Rect rectInteractionContainer;
		private Rect rectInteractionScrollView;

		private Rect rectFarewellButton;

		private Vector2 IWScrollPos;

		private int closeButtonOffsetXMax;
		private int closeButtonOffsetY;
		private int closeButtonSize;

		#endregion

		#region Merchant

		private Texture2D textureStoreWindow;
		private readonly Texture2D textureItemHighlight;
		//private Texture2D textureItemHighlightPressed;

		private Rect rectStoreWindow;
		private Rect rectItem;

		private bool mouseDown;

		#endregion

		#region Quest

		private readonly Texture2D textureXpIcon;
		private readonly Texture2D textureRenownIcon;
		private readonly Texture2D textureGoldIcon;

		#endregion

		#region Loot

		private readonly Texture2D textureGoldLoot;
		private Texture2D textureLootWindow;
		private readonly Texture2D textureLootItemContainer;

		private Rect rectLootWindow;
		private Rect rectLootGroup;
		private Rect rectLootContainer;
		private Rect rectLootScrollView;
		private Rect rectLootTakeAllButton;
		
		private string goldCount;

		#endregion

		#region Character

		/// <summary>
		/// character window
		/// </summary>
		private Action CW_Callback;

		private readonly List<string> CLStrings;

		private CharacterWindowType CWType;
		private bool showCharacterWindow;

		private Texture2D textureAbilitiesWindow;
		private Texture2D textureReputationWindow;
		private Texture2D textureCharacterInfo;

		private Rect rectCharacterWindow;

		private Rect rectCharacterTab;
		private Rect rectAbilitiesTab;
		private Rect rectReputationTab;

		private Rect rectCharacterStat;

		private Rect rectItemIcon;

		private float characterStatOffsetY;
		private float characterStatOffsetX;

		private int characterStatPosX, characterStatPosY;

		private int spellIconOffsetX, spellIconOffsetY;

		#endregion

		#region Journal

		/// <summary>
		/// journal list refresh
		/// </summary>
		Action JLR_Callback;

		/// <summary>
		/// journal content refresh
		/// </summary>
		Action JCR_Callback;

		private readonly List<Rect> JCLRects;
		private readonly List<string> JCLStrings;
		private readonly List<string> JLLStrings;

		private Texture2D textureJournalWindow;

		private Rect rectJournalWindow;
		private Rect rectJournalContentGroup;
		private Rect rectJournalContentContainer;
		private Rect rectJournalListGroup;
		private Rect rectJournalListContainer;

		private Rect rectJournalListScrollView;
		private Rect rectJournalContentScrollView;

		private Vector2 JLScrollPos;
		private Vector2 JCScrollPos;

		private int selectedQuestIndex;

		private bool showJournal;

		#endregion

		#region Options

		private bool showOptions;

		private Texture2D textureOptionsWindow;

		private Rect rectOptionsWindow;
		private Rect rectOptionsButton;

		#endregion

		#region Social

		/// <summary>
		/// social window
		/// </summary>
		private Action SW_Callback;

		private SocialWindowType SWType;
		private bool showSocialWindow;

		private Texture2D textureFriendsWindow;

		private Rect rectSocialWindow;
		private Rect rectSocialContent;

		#endregion

		#endregion

		#region Properties

		/// <summary>
		/// The Instance
		/// </summary>
		public static WindowManager Instance
		{
			get
			{
				return _instance ?? (_instance = new WindowManager());
			}
		}

		#endregion

		#region Constructors and Destructors

		private WindowManager()
		{
			this.ILRects = new List<Rect>();
			this.ILContents = new List<GUIContent>();
			this.ILStrings = new List<string>();

			this.CLStrings = new List<string>();

			this.JCLRects = new List<Rect>();
			this.JCLStrings = new List<string>();
			this.JLLStrings = new List<string>();
			
			this.guiSettings = GameManager.Instance.GuiSettings;

			textureLootItemContainer = (Texture2D)Resources.Load("gui/flt_item_container");
			textureGoldLoot = (Texture2D)Resources.Load("Items/Item0000");

			this.textureCoin = (Texture2D)Resources.Load("Icons/icon_misc_coin");
			this.textureItemHighlight = (Texture2D)Resources.Load("Misc/Highlight");
			// this.textureItemHighlightPressed = (Texture2D)Resources.Load("Misc/Highlight_Down");

			textureXpIcon = (Texture2D)Resources.Load("Icons/icon_item_xp");
			textureRenownIcon = (Texture2D)Resources.Load("Icons/icon_item_renown");
			textureGoldIcon = (Texture2D)Resources.Load("Icons/icon_item_gold");

			this.LoadGUISkin(HUD.SkidId);
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Sets up the window manager
		/// </summary>
		public void Setup(GameHandler gameHandler)
		{
			this.game = gameHandler;
			this.game.World.OnPlayerAdded += World_OnPlayerAdded;

			this.IWType = InteractionWindowType.WINDOW_NONE;
			this.CWType = CharacterWindowType.WINDOW_STATS;
			this.SWType = SocialWindowType.WINDOW_FRIENDS;

			this.IWScrollPos = Vector2.zero;

			this.mouseDown = false;
			this.showJournal = false;
			this.selectedQuestIndex = -1;

			this.CalculateGUIRects();
		}

		/// <summary>
		/// Shows talk menu window
		/// </summary>
		public void ShowDialogueMenuWindow(IList<MenuItemStructure> menu, string introMsg, MmoObject mmoObject, Action onReset)
		{
			if (IWType == InteractionWindowType.WINDOW_NONE)
			{
				this.IWType = InteractionWindowType.WINDOW_INTERACTION_MENU;

				this.ClearILs();

				var totalHeight = 0f;
				var width =  rectInteractionContainer.width - 16;

				var content = new GUIContent(introMsg);
				var height = this.guiSettings.GUIStyleWindowInteractionContent.CalcHeight(content, width);
				this.ILRects.Add(new Rect(5, 0, width, height));
				this.ILContents.Add(content);
				totalHeight += height + 5;

				foreach (var menuItem in menu)
				{
					switch ((MenuItemType)menuItem.ItemType)
					{
						case MenuItemType.Conversation:
							{
								var title = GameResources.Instance.LoadMessage(menuItem.ItemId, 0);
								content = new GUIContent(title, GetMenuItemIcon(MenuIconType.Conversation));
							}
							break;

						case MenuItemType.Quest:
							{
								QuestData quest;
								if (MmoItemDataStorage.Instance.TryGetQuest(menuItem.ItemId, out quest))
									content = new GUIContent(quest.Name, GetMenuItemIcon((MenuIconType)menuItem.IconType));
							}
							break;
					}

					height = this.guiSettings.GUIStyleButtonNpcChoiceList.CalcHeight(content, width); // 20 set in the guiStyle
					this.ILRects.Add(new Rect(5, totalHeight, 300, height));
					this.ILContents.Add(content);
					totalHeight += height;
				}

				this.rectInteractionScrollView = new Rect(0, 0, width, totalHeight + 10);

				this.IW_Callback = () => this.HandleDialogueMenuWindow(menu, mmoObject, onReset);
				HUD.ShowWindow(this.IW_Callback);
			}
		}

		/// <summary>
		/// Shows conversation window
		/// </summary>
		public void ShowDialogueWindow(string dialogue, MmoObject mmoObject, Action onReset)
		{
			if (IWType == InteractionWindowType.WINDOW_NONE)
			{
				this.IWType = InteractionWindowType.WINDOW_CONVERSATION;

				var height = this.guiSettings.GUIStyleWindowInteractionContent.CalcHeight(new GUIContent(dialogue), rectInteractionContainer.width - 16);
				this.ILRects.Add(new Rect(0, 0, rectInteractionContainer.width - 16, height));
				this.rectInteractionScrollView = new Rect(0, 0, rectInteractionContainer.width - 16, height);

				this.IW_Callback = () => this.HandleDialogueWindow(dialogue, mmoObject, onReset);
				HUD.ShowWindow(this.IW_Callback);
			}
		}

		/// <summary>
		/// Shows merchant window
		/// </summary>
		public void ShowMerchantWindow(IList<MmoItemData> items, MmoObject mmoObject, Action onReset)
		{
			if (IWType == InteractionWindowType.WINDOW_NONE)
			{
				this.IWType = InteractionWindowType.WINDOW_MERCHANT;
				this.mouseDown = false;

				HUD.Instance.OpenInventory();
				this.IW_Callback = () => this.HandleMerchantWindow(items, mmoObject, onReset);
				HUD.ShowWindow(this.IW_Callback);

				HUD.Instance.OpenInventory();
			}
		}

		/// <summary>
		/// Shows the loot window
		/// </summary>
		public void ShowLootWindow(Loot loot, MmoObject mmoObject, Action onReset)
		{
			if (IWType == InteractionWindowType.WINDOW_NONE)
			{
				this.IWType = InteractionWindowType.WINDOW_LOOT;
				this.mouseDown = false;

				this.goldCount = loot.Gold + " Gold";

				var items = loot.Count;
				if (loot.Gold > 0) items++;

				this.rectLootScrollView = new Rect(0, 0, rectLootContainer.width - 16, textureLootItemContainer.height * items);

				this.IW_Callback = () => this.HandleLootWindow(loot, mmoObject, onReset);
				HUD.ShowWindow(this.IW_Callback);
			}
		}

		/// <summary>
		/// Shows quest intro window
		/// </summary>
		public void ShowQuestIntroWindow(QuestData quest, MmoObject mmoObject, Action onAction)
		{
			if (IWType == InteractionWindowType.WINDOW_NONE)
			{
				this.IWType = InteractionWindowType.WINDOW_QUEST_INTRO;

				var tContent = new GUIContent(quest.Name);
				var totalHeight = 0f;

				var h1 = this.guiSettings.GUIStyleLabelQuestSubTitle.CalcHeight(tContent, this.rectInteractionContainer.width - 16);
				this.ILRects.Add(new Rect(0, totalHeight, rectInteractionContainer.width - 16, h1));
				totalHeight += h1 + 5;

				h1 = this.guiSettings.GUIStyleWindowInteractionContent.CalcHeight(new GUIContent(quest.QuestIntroMsg), rectInteractionContainer.width - 16);
				this.ILRects.Add(new Rect(5, totalHeight, rectInteractionContainer.width - 16, h1));
				totalHeight += h1 + 5;

				this.rectInteractionScrollView = new Rect(0, 0, rectInteractionContainer.width - 16, totalHeight + 10);

				this.IW_Callback = () => this.HandleQuestStartWindow(quest, mmoObject, onAction);
				HUD.ShowWindow(this.IW_Callback);
			}
		}

		/// <summary>
		/// Shows quest progress window
		/// </summary>
		public void ShowQuestProgressWindow(QuestData quest, MmoObject mmoObject, Action onAction)
		{
			if (IWType == InteractionWindowType.WINDOW_NONE)
			{
				this.IWType = InteractionWindowType.WINDOW_QUEST_PROGRESS;

				var height = this.guiSettings.GUIStyleWindowInteractionContent.CalcHeight(new GUIContent(quest.QuestProgressMsg), rectInteractionContainer.width - 16);
				this.ILRects.Add(new Rect(0, 0, rectInteractionContainer.width - 16, height));
				this.rectInteractionScrollView = new Rect(0, 0, rectInteractionContainer.width - 16, height);

				this.IW_Callback = () => this.HandleQuestProgressWindow(quest, mmoObject, onAction);
				HUD.ShowWindow(this.IW_Callback);
			}
		}

		/// <summary>
		/// Shows quest complete window
		/// </summary>
		public void ShowQuestCompleteWindow(QuestData quest, MmoObject mmoObject, Action onAction)
		{
			if (IWType == InteractionWindowType.WINDOW_NONE)
			{
				this.IWType = InteractionWindowType.WINDOW_QUEST_COMPLETE;

				var tContent = new GUIContent(quest.Name);
				var totalHeight = 0f;

				var h1 = this.guiSettings.GUIStyleLabelQuestSubTitle.CalcHeight(tContent, rectInteractionContainer.width - 16);
				this.ILRects.Add(new Rect(0, totalHeight, rectInteractionContainer.width - 16, h1));
				totalHeight += h1 + 5;

				h1 = this.guiSettings.GUIStyleWindowInteractionContent.CalcHeight(new GUIContent(quest.QuestCompleteMsg), rectInteractionContainer.width - 16);
				this.ILRects.Add(new Rect(5, totalHeight, rectInteractionContainer.width - 16, h1));
				totalHeight += h1 + 25;

				h1 = this.ILRects[0].height;
				this.ILRects.Add(new Rect(0, totalHeight, rectInteractionContainer.width - 16, h1));
				totalHeight += h1 + 10;

				float rowFloat = 0;

				var optItms = quest.RewardOptionalItems;
				if (optItms != null)
				{
					foreach (var item in optItms)
					{
						var itemId = item.ItemId;

						MmoItemData itemData;
						if (MmoItemDataStorage.Instance.TryGetMmoItem(itemId, out itemData))
						{
							this.ILContents.Add(itemData.DrawContent);
							this.ILStrings.Add(itemData.Name);

							rowFloat += 0.5f;
						}
					}
				}

				var itms = quest.RewardItems;
				if (itms != null)
				{
					foreach (var item in itms)
					{
						var itemId = item.ItemId;

						MmoItemData itemData;
						if (MmoItemDataStorage.Instance.TryGetMmoItem(itemId, out itemData))
						{
							this.ILContents.Add(itemData.DrawContent);
							this.ILStrings.Add(itemData.Name);

							rowFloat += 0.5f;
						}
					}
				}

				var spells = quest.RewardSpells;
				if (spells != null)
				{
					foreach (var spellId in spells)
					{
						SpellData spell;
						if (MmoItemDataStorage.Instance.TryGetSpell(spellId, out spell))
						{
							this.ILContents.Add(spell.DrawContent);
							this.ILStrings.Add(spell.Name);

							rowFloat += 0.5f;
						}
					}
				}

				if (quest.RewardXp > 0)
				{
					this.ILContents.Add(new GUIContent(textureXpIcon));
					this.ILStrings.Add(quest.RewardXp + " XP");

					rowFloat += 0.5f;
				}

				if (quest.RewardMoney > 0)
				{
					this.ILContents.Add(new GUIContent(textureGoldIcon));
					this.ILStrings.Add(quest.RewardMoney + " Gold");

					rowFloat += 0.5f;
				}

				if (quest.RewardRenown > 0)
				{
					this.ILContents.Add(new GUIContent(textureRenownIcon));
					this.ILStrings.Add(quest.RewardRenown + " RP");

					rowFloat += 0.5f;
				}

				var rowInt = Mathf.CeilToInt(rowFloat);
				totalHeight += rowInt * 40;

				this.rectInteractionScrollView = new Rect(0, 0, rectInteractionContainer.width - 16, totalHeight + 10);

				this.IW_Callback = () => this.HandleQuestCompleteWindow(quest, mmoObject, onAction);
				HUD.ShowWindow(this.IW_Callback);
			}
		}

		/// <summary>
		/// Closes the current interaction window
		/// </summary>
		public void CloseInteractionWindow()
		{
			if (IWType != InteractionWindowType.WINDOW_NONE)
			{
				if (IWType == InteractionWindowType.WINDOW_MERCHANT)
					HUD.Instance.CloseInventory();

				HUD.CloseWindow(this.IW_Callback);
				this.IWType = InteractionWindowType.WINDOW_NONE;
				this.IW_Callback = null;

				this.ClearILs();

				this.IWScrollPos = Vector2.zero;
			}
		}

		/// <summary>
		/// Toggles character window
		/// </summary>
		public void ToggleCharacterWindow()
		{
			if (!showCharacterWindow)
			{
				switch (CWType)
				{
					case CharacterWindowType.WINDOW_STATS:
						ShowStatsWindow();
						break;

					case CharacterWindowType.WINDOW_ABILITIES:
						ShowAbilitiesWindow();
						break;

					case CharacterWindowType.WINDOW_REPUTATION:
						ShowReputationWindow();
						break;
				}
			}
			else
			{
				CloseCharacterWindow();
			}
		}

		/// <summary>
		/// Closes the character window
		/// </summary>
		public void CloseCharacterWindow()
		{
			if (showCharacterWindow)
			{
				HUD.CloseWindow(this.CW_Callback);
				this.CW_Callback = null;

				this.CLStrings.Clear();

				this.showCharacterWindow = false;
			}
		}

		/// <summary>
		/// Shows stats window
		/// </summary>
		void ShowStatsWindow()
		{
			if (!showCharacterWindow)
			{
				this.CWType = CharacterWindowType.WINDOW_STATS;
				this.CW_Callback = this.HandleStatsWindow;

				this.showCharacterWindow = true;
				HUD.ShowWindow(CW_Callback);
			}
		}

		/// <summary>
		/// Shows abilities window
		/// </summary>
		void ShowAbilitiesWindow()
		{
			if (!showCharacterWindow)
			{
				this.CWType = CharacterWindowType.WINDOW_ABILITIES;
				this.CW_Callback = this.HandleAbilitiesWindow;

				this.showCharacterWindow = true;
				HUD.ShowWindow(CW_Callback);
			}
		}

		/// <summary>
		/// Shows reputation window
		/// </summary>
		void ShowReputationWindow()
		{
			if (!showCharacterWindow)
			{
				this.CWType = CharacterWindowType.WINDOW_REPUTATION;
				this.CW_Callback = this.HandleReputationWindow;

				this.showCharacterWindow = true;
				HUD.ShowWindow(CW_Callback);
			}
		}

		/// <summary>
		/// Toggles the journal window
		/// </summary>
		public void ToggleJournalWindow()
		{
			if (!showJournal)
			{
				(JLR_Callback = () =>
				{
					this.JLLStrings.Clear();
					
					var quests = player.QuestManager.GetQuests();
					foreach (var activeQuest in quests)
					{
						if (activeQuest == null)
							continue;
						
						QuestData quest;
						if (MmoItemDataStorage.Instance.TryGetQuest(activeQuest.QuestId, out quest))
						{
							var questName = quest.Name;
							if (activeQuest.Status == QuestStatus.TurnIn)
								questName += " (Complete)";

							this.JLLStrings.Add(questName);
						}
					}
					})();

				(JCR_Callback = () =>
					{
						if (selectedQuestIndex != -1)
						{
							this.JCLRects.Clear();
							this.JCLStrings.Clear();

							var activeQuest = player.QuestManager.GetQuestByIndex(selectedQuestIndex);

							QuestData quest;
							if (MmoItemDataStorage.Instance.TryGetQuest(activeQuest.QuestId, out quest))
							{
								var tContent = new GUIContent(quest.Name);
								var totalHeight = 0f;
								var width = rectJournalContentContainer.width - 16;

								var h1 = this.guiSettings.GUIStyleLabelQuestSubTitle.CalcHeight(tContent, width);
								this.JCLRects.Add(new Rect(0, totalHeight, width, h1)); // 0
								this.JCLStrings.Add(quest.Name);
								totalHeight += h1 + 5;

								h1 = this.guiSettings.GUIStyleWindowInteractionContent.CalcHeight(new GUIContent(quest.QuestIntroMsg), width);
								this.JCLRects.Add(new Rect(5, totalHeight, width, h1)); // 1
								this.JCLStrings.Add(quest.QuestIntroMsg);
								totalHeight += h1 + 25;

								h1 = JCLRects[0].height;
								this.JCLRects.Add(new Rect(0, totalHeight, width, h1)); // 2
								this.JCLStrings.Add("Objectives");
								totalHeight += h1 + 5;

								h1 = this.guiSettings.GUIStyleWindowInteractionContent.CalcHeight(tContent, width);
								if (activeQuest.Status == QuestStatus.TurnIn)
								{
									this.JCLRects.Add(new Rect(5, totalHeight, width, h1));
									this.JCLStrings.Add(quest.FinalObjective);

									totalHeight += h1 + 2;
								}
								else
								{
									for (var i = 0; i < quest.Objectives.Length; i++)
									{
										this.JCLRects.Add(new Rect(5, totalHeight, width, h1));

										var reqCount = quest.ObjectiveCounts[i];
										var actCount = activeQuest.Counts[i];
										string objective = string.Empty;

										if (reqCount == actCount)
											objective = "(C) ";

										if (reqCount > 1)
										{
											objective = string.Format("{0}{1}: {2} / {3}", objective, quest.Objectives[i], actCount, reqCount);
										}
										else
										{
											objective += quest.Objectives[i];
										}

										this.JCLStrings.Add(objective);

										totalHeight += h1 + 2;
									}
								}

								this.rectJournalContentScrollView = new Rect(0, 0, width, totalHeight + 10);
							}
						}
					})();

				this.showJournal = true;
				HUD.ShowWindow(HandleJournalWindow);
			}
			else
			{
				CloseJournalWindow();
			}
		}

		/// <summary>
		/// Refreshes the journal menu
		/// </summary>
		public void RefreshJournalMenu()
		{
			if (JLR_Callback != null)
			{
				this.JLR_Callback();

				var count = player.QuestManager.Count;
				if (selectedQuestIndex >= count)
				{
					this.selectedQuestIndex = count - 1;
					this.JCR_Callback();
				}
			}
		}

		/// <summary>
		/// Closes the journal window
		/// </summary>
		public void CloseJournalWindow()
		{
			if (showJournal)
			{
				HUD.CloseWindow(HandleJournalWindow);
				this.JCR_Callback = null;
				this.JLR_Callback = null;

				this.showJournal = false;

				this.JCLRects.Clear();
				this.JCLStrings.Clear();
				this.JLLStrings.Clear();

				this.JCScrollPos = Vector2.zero;
				this.JLScrollPos = Vector2.zero;
			}
		}

		/// <summary>
		/// Toggles options window
		/// </summary>
		public void ToggleOptionsWindow()
		{
			this.showOptions = !this.showOptions;
			if (showOptions)
			{
				HUD.ShowWindow(HandleOptionsWindow);
			}
			else
			{
				HUD.CloseWindow(HandleOptionsWindow);
			}
		}

		/// <summary>
		/// Toggles social window
		/// </summary>
		public void ToggleSocialWindow()
		{
			if (!showSocialWindow)
			{
				switch (SWType)
				{
					case SocialWindowType.WINDOW_FRIENDS:
						{
							ShowFriendsWindow();
						}
						break;
				}
			}
			else
			{
				CloseSocialWindow();
			}
		}

		/// <summary>
		/// Closes the social window
		/// </summary>
		public void CloseSocialWindow()
		{
			if (showSocialWindow)
			{
				HUD.CloseWindow(this.SW_Callback);
				this.SW_Callback = null;

				this.showSocialWindow = false;
			}
		}

		/// <summary>
		/// Shows friends window
		/// </summary>
		void ShowFriendsWindow()
		{
			if (!showSocialWindow)
			{
				this.SWType = SocialWindowType.WINDOW_FRIENDS;
				this.SW_Callback = this.HandleFriendsWindow;

				this.showSocialWindow = true;
				HUD.ShowWindow(SW_Callback);
			}
		}

		#endregion

		#region Window Handlers

		void HandleDialogueMenuWindow(IEnumerable<MenuItemStructure> menu, MmoObject mmoObject, Action onReset)
		{
			GUIX.Window((int)WinId.WIN_ID_INTERACTION_MENU, rectInteractionWindow, textureInteractionWindow, this.guiSettings.GUIStyleWindow);
			GUI.Label(rectInteractionWindowTitle, mmoObject.Name, this.guiSettings.GUIStyleWindowTitle);

			GUI.BeginGroup(rectInteractionGroup);
			{
				this.IWScrollPos = GUI.BeginScrollView(this.rectInteractionContainer, this.IWScrollPos, this.rectInteractionScrollView);
				{
					var rect = ILRects[0];
					GUI.Label(rect, ILContents[0], this.guiSettings.GUIStyleWindowInteractionContent);

					var i = 1;
					foreach (var menuItem in menu)
					{
						if (GUIX.Button(ILRects[i], this.ILContents[i++], this.guiSettings.GUIStyleButtonNpcChoiceList))
						{
							// Note: this clears all the layout lists so we MUST break out of the loop
							// to prevent exception
							CloseInteractionWindow();

							switch ((MenuIconType)menuItem.IconType)
							{
								case MenuIconType.QuestActive:
									{
										QuestData quest;
										if (MmoItemDataStorage.Instance.TryGetQuest(menuItem.ItemId, out quest))
											this.ShowQuestIntroWindow(quest, mmoObject, onReset);
									}
									break;

								case MenuIconType.QuestInProgress:
									{
										QuestData quest;
										if (MmoItemDataStorage.Instance.TryGetQuest(menuItem.ItemId, out quest))
											this.ShowQuestProgressWindow(quest, mmoObject, onReset);
									}
									break;

								case MenuIconType.QuestTurnIn:
									{
										QuestData quest;
										if (MmoItemDataStorage.Instance.TryGetQuest(menuItem.ItemId, out quest))
											this.ShowQuestCompleteWindow(quest, mmoObject, onReset);
									}
									break;

								case MenuIconType.Conversation:
									{
										Operations.SelectDialogue(menuItem.ItemId);
										var msg = GameResources.Instance.LoadMessage(menuItem.ItemId, 1);
										this.ShowDialogueWindow(msg, mmoObject, onReset);
									}
									break;
							}

							break;
						}
					}
				}
				GUI.EndScrollView();
			}
			GUI.EndGroup();

			if (GUIX.Button(rectFarewellButton, "Farewell"))
			{
				onReset();
			}

			if (GUIX.Button(new Rect(rectInteractionWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectInteractionWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				onReset();
			}
		}

		void HandleDialogueWindow(string dialogue, MmoObject mmoObject, Action onReset)
		{
			GUIX.Window((int)WinId.WIN_ID_CONVERSATION, rectInteractionWindow, textureInteractionWindow, this.guiSettings.GUIStyleWindow);
			GUI.Label(rectInteractionWindowTitle, mmoObject.Name, this.guiSettings.GUIStyleWindowTitle);

			GUI.BeginGroup(rectInteractionGroup);
			{
				this.IWScrollPos = GUI.BeginScrollView(this.rectInteractionContainer, this.IWScrollPos, this.rectInteractionScrollView);
				{
					GUI.Label(ILRects[0], dialogue, this.guiSettings.GUIStyleWindowInteractionContent);
				}
				GUI.EndScrollView();
			}
			GUI.EndGroup();

			if (GUIX.Button(rectFarewellButton, "Farewell"))
			{
				onReset();
			}

			if (GUIX.Button(new Rect(rectInteractionWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectInteractionWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				onReset();
			}
		}

		void HandleMerchantWindow(IEnumerable<MmoItemData> items, MmoObject mmoObject, Action onReset)
		{
			GUIX.Window((int)WinId.WIN_ID_MERCHANT, rectStoreWindow, textureStoreWindow, this.guiSettings.GUIStyleWindow);

			switch (Event.current.type)
			{
				case EventType.MouseDown:
					this.mouseDown = true;
					break;
				case EventType.MouseUp:
					this.mouseDown = false;
					break;
			}

			var index = 0;
			var row = 0;
			var column = 0;

			foreach (var mmoItemData in items)
			{
				rectItem.x = rectStoreWindow.x + 21 + column * 191;
				rectItem.y = rectStoreWindow.y + 62 + row * 60;

				GUI.Label(new Rect(rectItem.x + 53, rectItem.y, 108, 40), mmoItemData.Name, this.guiSettings.GUIStyleMerchantItemName);
				GUI.Label(new Rect(rectItem.x + 55, rectItem.y + 30, 108, 9), textureCoin, this.guiSettings.GUIStyleMerchantItemPrice);
				GUIX.NumLabel(new Rect(rectItem.x + 55, rectItem.y + 27, 108 - textureCoin.width, 15), mmoItemData.BuyoutPrice, this.guiSettings.GUIStyleMerchantItemPrice);

				if (GUIX.TooltipButton(rectItem, mmoItemData.DrawContent, this.guiSettings.GUIStyleButtonPlain))
				{
					if (Event.current.button == GameSettings.RIGHT_MOUSE_BUTTON)
					{
						if (player.Money >= mmoItemData.BuyoutPrice)
						{
							const byte count = 1;
							if (player.Inventory.CanAddItem(mmoItemData.ItemId, count))
							{
								Operations.PurchaseItem((byte) index, count);
							}
							else
							{
								this.game.Chat.PostMessage(MessageType.Game, "Inventory full");
							}
						}
						else
						{
							this.game.Chat.PostMessage(MessageType.Game, "Insufficient fund");
						}
					}
				}

				if (rectItem.Contains(Event.current.mousePosition))
				{
					GUI.DrawTexture(rectItem, textureItemHighlight);
					//if (mouseDown)
					//{
					//	GUI.DrawTexture(rectItem, textureItemHighlightPressed);
					//}
					//else
					//{
					//	GUI.DrawTexture(rectItem, textureItemHighlight);
					//}
				}

				index++;
				if (index % 2 == 0)
					row++;
				else
					column++;
			}

			if (GUIX.Button(new Rect(rectStoreWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectStoreWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				onReset();
			}
		}

		void HandleLootWindow(Loot loot, MmoObject mmoObject, Action onReset)
		{
			if (loot == null || loot.IsEmpty)
			{
				onReset();
				mmoObject.SetLoot(null);
				return;
			}

			GUIX.Window((int)WinId.WIN_ID_LOOT, rectLootWindow, textureLootWindow, this.guiSettings.GUIStyleWindow);

			GUI.BeginGroup(rectLootGroup);
			{
				this.IWScrollPos = GUI.BeginScrollView(rectLootContainer, this.IWScrollPos, rectLootScrollView);
				{
					var rectLootItemHolder = new Rect(0, 0, textureLootItemContainer.width, textureLootItemContainer.height);
					var rectLootItem = new Rect(2, 7, GameSettings.ITEM_ICON_SIZE, GameSettings.ITEM_ICON_SIZE);
					var rectLootInfo = new Rect(56, 9, 108, 42);

					if (loot.Gold > 0)
					{
						GUI.DrawTexture(rectLootItemHolder, textureLootItemContainer);

						if (GUIX.Button(rectLootItem, this.textureGoldLoot, this.guiSettings.GUIStyleButtonPlain) ||
							GUIX.Button(rectLootInfo, this.goldCount, this.guiSettings.GUIStyleButtonLootName))
						{
							loot.CollectGold();
						}

						if (rectLootItem.Contains(Event.current.mousePosition))
						{
							GUI.DrawTexture(rectLootItem, textureItemHighlight);
						}

						rectLootItemHolder.y += rectLootItemHolder.height;
						rectLootInfo.y = rectLootItemHolder.y + 8;
						rectLootItem.y = rectLootItemHolder.y + 7;
					}

					var nls = loot.GetLoots();
					if (nls != null)
					{
						var offset = new Vector2(rectLootGroup.x, rectLootGroup.y);
						for (var i = 0; i < nls.Length; i++)
						{
							var itemInfo = nls[i];
							if (itemInfo.Status != LootItemStatus.Unlooted)
								continue;

							GUI.DrawTexture(rectLootItemHolder, textureLootItemContainer);

							var item = itemInfo.Item;
							if (GUIX.TooltipButton(rectLootItem, offset, item.DrawContent, this.guiSettings.GUIStyleButtonPlain) ||
								GUIX.Button(rectLootInfo, item.Name, this.guiSettings.GUIStyleButtonLootName))
							{
								if (player.Inventory.CanAddItem(item.ItemId, itemInfo.Count))
								{
									loot.CollectLootItem(i);
								}
								else
								{
									ChatManager.Instance.PostMessage(MessageType.Game, "Inventory full");
								}
							}

							GUIX.NumLabel(rectLootItem, itemInfo.Count, this.guiSettings.GUIStyleLabelStackCount);

							if (rectLootItem.Contains(Event.current.mousePosition))
							{
								GUI.DrawTexture(rectLootItem, textureItemHighlight);
							}

							rectLootItemHolder.y += rectLootItemHolder.height;
							rectLootInfo.y = rectLootItemHolder.y + 8;
							rectLootItem.y = rectLootItemHolder.y + 7;
						}
					}
				}
				GUI.EndScrollView();
			}
			GUI.EndGroup();

			if (GUIX.Button(rectLootTakeAllButton, "Take All"))
			{
				loot.CollectAll();
				onReset();
			}
			
			if (GUIX.Button(new Rect(rectLootWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectLootWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				onReset();
			}
		}

		void HandleQuestStartWindow(QuestData quest, MmoObject mmoObject, Action onReset)
		{
			GUIX.Window((int)WinId.WIN_ID_QUEST_INTRO, rectInteractionWindow, textureInteractionWindow, this.guiSettings.GUIStyleWindow);
			GUI.Label(rectInteractionWindowTitle, quest.Name, this.guiSettings.GUIStyleWindowTitle);

			GUI.BeginGroup(rectInteractionGroup);
			{
				this.IWScrollPos = GUI.BeginScrollView(this.rectInteractionContainer, this.IWScrollPos, this.rectInteractionScrollView);
				{
					GUI.Label(ILRects[0], quest.Name, this.guiSettings.GUIStyleLabelQuestSubTitle);
					GUI.Label(ILRects[1], quest.QuestIntroMsg, this.guiSettings.GUIStyleWindowInteractionContent);
				}
				GUI.EndScrollView();
			}
			GUI.EndGroup();

			if (GUIX.Button(rectFarewellButton, "Accept"))
			{
				Operations.AcceptQuest(quest.QuestId);
				onReset();
			}

			if (GUIX.Button(new Rect(rectInteractionWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectInteractionWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				onReset();
			}
		}

		void HandleQuestProgressWindow(QuestData quest, MmoObject mmoObject, Action onReset)
		{
			GUIX.Window((int)WinId.WIN_ID_QUEST_PROGRESS, rectInteractionWindow, textureInteractionWindow, this.guiSettings.GUIStyleWindow);
			GUI.Label(rectInteractionWindowTitle, quest.Name, this.guiSettings.GUIStyleWindowTitle);

			GUI.BeginGroup(rectInteractionGroup);
			{
				this.IWScrollPos = GUI.BeginScrollView(this.rectInteractionContainer, this.IWScrollPos, this.rectInteractionScrollView);
				{
					GUI.Label(ILRects[0], quest.QuestProgressMsg, this.guiSettings.GUIStyleWindowInteractionContent);
				}
				GUI.EndScrollView();
			}
			GUI.EndGroup();

			if (GUIX.Button(rectFarewellButton, "Done"))
			{
				onReset();
			}

			if (GUIX.Button(new Rect(rectInteractionWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectInteractionWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				onReset();
			}
		}

		void HandleQuestCompleteWindow(QuestData quest, MmoObject mmoObject, Action onReset)
		{
			GUIX.Window((int)WinId.WIN_ID_QUEST_COMPLETE, rectInteractionWindow, textureInteractionWindow, this.guiSettings.GUIStyleWindow);
			GUI.Label(rectInteractionWindowTitle, quest.Name, this.guiSettings.GUIStyleWindowTitle);

			GUI.BeginGroup(rectInteractionGroup);
			{
				this.IWScrollPos = GUI.BeginScrollView(this.rectInteractionContainer, this.IWScrollPos, this.rectInteractionScrollView);
				{
					GUI.Label(ILRects[0], quest.Name, this.guiSettings.GUIStyleLabelQuestSubTitle);
					GUI.Label(ILRects[1], quest.QuestCompleteMsg, this.guiSettings.GUIStyleWindowInteractionContent);
					GUI.Label(ILRects[2], "Rewards", this.guiSettings.GUIStyleLabelQuestSubTitle);

					var cId = 0;
					var x = 0;

					var lastRect = ILRects[2];
					if (quest.RewardOptionalItems != null)
					{
					}

					if (quest.RewardSpells != null)
					{
					}

					if (quest.RewardItems != null)
					{
						lastRect.y += lastRect.height - 30;
						var offset = new Vector2(rectInteractionGroup.x, rectInteractionGroup.y);

						for (int i = 0; i < quest.RewardItems.Length; i++)
						{
							lastRect = new Rect(5 + (x % 2) * 150, lastRect.y + (++x % 2) * 40, 48, 48);
							GUIX.TooltipLabel(lastRect, offset, ILContents[cId]);
							GUI.Label(new Rect(lastRect.xMax + 2, lastRect.y, 100, lastRect.height), ILStrings[cId++], this.guiSettings.GUIStyleButtonQuestReward);
						}

						x = (x % 2 == 1) ? x + 1 : x; // bring to next line if odd # of items
						lastRect.y += 15;
					}

					if (quest.RewardXp > 0)
					{
						lastRect = new Rect(5 + (x % 2) * 150, lastRect.y + (++x % 2) * 40, 50, 40);
						GUI.Label(lastRect, ILContents[cId]);
						GUI.Label(new Rect(lastRect.xMax, lastRect.y, 100, lastRect.height), ILStrings[cId++], this.guiSettings.GUIStyleButtonQuestReward);
					}

					if (quest.RewardMoney > 0)
					{
						lastRect = new Rect(5 + (x % 2) * 150, lastRect.y + (++x % 2) * 40, 50, 40);
						GUI.Label(lastRect, ILContents[cId]);
						GUI.Label(new Rect(lastRect.xMax, lastRect.y, 100, lastRect.height), ILStrings[cId++], this.guiSettings.GUIStyleButtonQuestReward);
					}

					if (quest.RewardRenown > 0)
					{
						lastRect = new Rect(5 + (x % 2) * 150, lastRect.y + (++x % 2) * 40, 50, 40);
						GUI.Label(lastRect, ILContents[cId]);
						GUI.Label(new Rect(lastRect.xMax, lastRect.y, 100, lastRect.height), ILStrings[cId], this.guiSettings.GUIStyleButtonQuestReward);
					}
				}
				GUI.EndScrollView();
			}
			GUI.EndGroup();

			if (GUIX.Button(rectFarewellButton, "Finish"))
			{
				Operations.TurnInQuest(quest.QuestId);
				onReset();
			}

			if (GUIX.Button(new Rect(rectInteractionWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectInteractionWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				onReset();
			}
		}

		void HandleStatsWindow()
		{
			GUIX.Window((int)WinId.WIN_ID_CHAR_STATS, rectCharacterWindow, textureCharacterInfo, this.guiSettings.GUIStyleWindow);

			rectCharacterStat.x = characterStatPosX;
			rectCharacterStat.y = characterStatPosY;

			// max health
			GUIX.NumLabel(rectCharacterStat, player.CalculateStat(Stats.Health), this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// max power
			GUIX.NumLabel(rectCharacterStat, player.CalculateStat(Stats.Power), this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// weapon damage
			rectCharacterStat.y += characterStatOffsetY;
			GUIX.NumLabel(rectCharacterStat, player.CalculateStat(Stats.WeaponDamage), this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// armor
			GUIX.NumLabel(rectCharacterStat, player.CalculateStat(Stats.Armor), this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// fame
			rectCharacterStat.y += characterStatOffsetY;
			GUIX.NumLabel(rectCharacterStat, 0, this.guiSettings.GUIStyleCharacterStatValue);

			rectCharacterStat.x = characterStatPosX + characterStatOffsetX;
			rectCharacterStat.y = characterStatPosY;

			// health recovery
			GUIX.NumLabel(rectCharacterStat, 0, this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// power recovery
			GUIX.NumLabel(rectCharacterStat, 0, this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// spell damage
			rectCharacterStat.y += characterStatOffsetY;
			GUIX.NumLabel(rectCharacterStat, player.CalculateStat(Stats.SpellDamage), this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// absorption
			GUIX.NumLabel(rectCharacterStat, player.CalculateStat(Stats.Absorption), this.guiSettings.GUIStyleCharacterStatValue);
			rectCharacterStat.y += characterStatOffsetY;

			// infamy
			rectCharacterStat.y += characterStatOffsetY;
			GUIX.NumLabel(rectCharacterStat, 0, this.guiSettings.GUIStyleCharacterStatValue);

			if (GUIX.Button(rectAbilitiesTab, "Abilities", this.guiSettings.GUIStyleButtonTabs))
			{
				this.CloseCharacterWindow();
				this.ShowAbilitiesWindow();
			}

			if (GUIX.Button(rectReputationTab, "Reputation", this.guiSettings.GUIStyleButtonTabs))
			{
				this.CloseCharacterWindow();
				this.ShowReputationWindow();
			}

			if (GUIX.Button(new Rect(rectCharacterWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectCharacterWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				this.CloseCharacterWindow();
			}
		}

		void HandleAbilitiesWindow()
		{
			GUIX.Window((int)WinId.WIN_ID_CHAR_ABILITIES, rectCharacterWindow, textureAbilitiesWindow, this.guiSettings.GUIStyleWindow);

			var x = 0;
			var y = 0;

			foreach (var spell in this.player.SpellManager.Spells)
			{
				var rectName = new Rect(rectCharacterWindow.x + 74 + x * 188, rectCharacterWindow.y + 59 + y * 60, 109, 40);

				rectItemIcon.x = this.spellIconOffsetX + x * 188;
				rectItemIcon.y = this.spellIconOffsetY + y * 60;

				var spellData = spell.SpellData;
				GUIX.TooltipLabel(rectItemIcon, spellData.DrawContent, this.guiSettings.GUIStyleButtonPlain);
				GUI.Label(rectName, spell.Name, this.guiSettings.GUIStyleCharacterSpellItemName);

				var currEvent = Event.current;
				if (currEvent.type == EventType.MouseDrag && currEvent.button == GameSettings.LEFT_MOUSE_BUTTON && !HUD.Instance.IsDragging)
					if (rectItemIcon.Contains(currEvent.mousePosition))
						HUD.Instance.BeginDrag(new DraggableSpell { ItemId = spell.Id, Icon = spellData.Icon });

				x++;
				if (x % 2 == 0)
				{
					x = 0;
					y++;
				}
			}

			if (GUIX.Button(rectCharacterTab, "Character", this.guiSettings.GUIStyleButtonTabs))
			{
				this.CloseCharacterWindow();
				this.ShowStatsWindow();
			}

			if (GUIX.Button(rectReputationTab, "Reputation", this.guiSettings.GUIStyleButtonTabs))
			{
				this.CloseCharacterWindow();
				this.ShowReputationWindow();
			}

			if (GUIX.Button(new Rect(rectCharacterWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectCharacterWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				this.CloseCharacterWindow();
			}
		}

		void HandleReputationWindow()
		{
			GUIX.Window((int)WinId.WIN_ID_CHAR_REPUTATION, rectCharacterWindow, textureReputationWindow, this.guiSettings.GUIStyleWindow);

			if (GUIX.Button(rectCharacterTab, "Character", this.guiSettings.GUIStyleButtonTabs))
			{
				this.CloseCharacterWindow();
				this.ShowStatsWindow();
			}

			if (GUIX.Button(rectAbilitiesTab, "Abilities", this.guiSettings.GUIStyleButtonTabs))
			{
				this.CloseCharacterWindow();
				this.ShowAbilitiesWindow();
			}

			if (GUIX.Button(new Rect(rectCharacterWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectCharacterWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				this.CloseCharacterWindow();
			}
		}

		void HandleJournalWindow()
		{
			GUIX.Window((int)WinId.WIN_ID_JOURNAL, rectJournalWindow, textureJournalWindow, this.guiSettings.GUIStyleWindow);

			GUI.BeginGroup(rectJournalListGroup);
			{
				for (int i = 0; i < JLLStrings.Count; i++)
				{
					if (GUIX.SelectButton(new Rect(0, 0 + i * 23, 285, 21), selectedQuestIndex == i, JLLStrings[i], this.guiSettings.GUIStyleButtonSelection))
					{
						this.selectedQuestIndex = i;
						this.JCR_Callback();
					}
				}
			}
			GUI.EndGroup();

			if (selectedQuestIndex != -1)
			{
				GUI.BeginGroup(rectJournalContentGroup);
				{
					this.JCScrollPos = GUI.BeginScrollView(this.rectJournalContentContainer, this.JCScrollPos, this.rectJournalContentScrollView);
					{
						GUI.Label(JCLRects[0], JCLStrings[0], this.guiSettings.GUIStyleLabelQuestSubTitle); // quest name
						GUI.Label(JCLRects[1], JCLStrings[1], this.guiSettings.GUIStyleWindowInteractionContent); // intro
						GUI.Label(JCLRects[2], JCLStrings[2], this.guiSettings.GUIStyleLabelQuestSubTitle); // objective

						for (int i = 3; i < JCLRects.Count; i++)
						{
							GUI.Label(JCLRects[i], JCLStrings[i], this.guiSettings.GUIStyleWindowInteractionContent);
						}
					}
					GUI.EndScrollView();
				}
				GUI.EndGroup();
			}

			if (GUIX.Button(new Rect(rectJournalWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectJournalWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				this.CloseJournalWindow();
			}
		}

		void HandleOptionsWindow()
		{
			GUIX.Window((int)WinId.WIN_ID_OPTIONS, rectOptionsWindow, textureOptionsWindow, this.guiSettings.GUIStyleWindow);
			
			var x = rectOptionsWindow.x + 14;
			var y = rectOptionsWindow.y + 44;

			if (GUIX.Button(new Rect(x, y, 150, 30), "Resume"))
			{
				this.ToggleOptionsWindow();
			}

			GUIX.Button(new Rect(x, y += 34, 150, 30), "Settings");
			GUIX.Button(new Rect(x, y += 34, 150, 30), "Help");

			if (GUIX.Button(new Rect(x, y + 55, 150, 30), "Logout"))
			{
				this.ToggleOptionsWindow();
				GameManager.Instance.Disconnect();
			}
		}

		void HandleFriendsWindow()
		{
			GUIX.Window((int) WinId.WIN_ID_SOCIAL_FRIENDS, rectSocialWindow, textureFriendsWindow, this.guiSettings.GUIStyleWindow);

			GUI.BeginGroup(rectSocialContent);
			{
				var i = 0;
				foreach (var nameOfRequester in player.SocialManager.ReceivedFriendRequests)
				{
					var rect = new Rect(0, i++ * 23, rectSocialContent.width, 21);
					GUI.Label(rect, nameOfRequester, this.guiSettings.GUIStyleLabelFriendRequest);

					if (GUIX.Button(new Rect(rect.xMax - 18, rect.y + 4, 12, 12), string.Empty, guiSettings.GUIStyleButtonDecline))
					{
						var requester = nameOfRequester;
						Operations.DeclineFriendRequest(requester);
						GameManager.Instance.ExecuteAtNextFrame(() => player.SocialManager.ReceivedFriendRequests.Remove(requester));
					}
					if (GUIX.Button(new Rect(rect.xMax - 34, rect.y + 4, 12, 12), string.Empty, guiSettings.GUIStyleButtonAccept))
					{
						var requester = nameOfRequester;
						Operations.AcceptFriendRequest(requester);
						GameManager.Instance.ExecuteAtNextFrame(() => player.SocialManager.ReceivedFriendRequests.Remove(requester));
					}
				}

				foreach (var friend in player.SocialManager.OnlineFriends)
				{
					GUIX.Button(new Rect(0, i * 23, rectSocialContent.width, 21), friend.ToDescriptionString(), this.guiSettings.GUIStyleButtonSelection);

					var icon = GetSocialStatusIcon(friend.OnlineStatus);
					if (Event.current.type == EventType.Repaint)
						Graphics.DrawTexture(new Rect(5, 7 + i++ * 23, icon.width, icon.height), icon);
				}

				foreach (var friend in player.SocialManager.OfflineFriends)
				{
					GUIX.Button(new Rect(0, i * 23, rectSocialContent.width, 21), friend.ToDescriptionString(), this.guiSettings.GUIStyleButtonSelection);

					var icon = GetSocialStatusIcon(friend.OnlineStatus);
					if (Event.current.type == EventType.Repaint)
						Graphics.DrawTexture(new Rect(5, 7 + i++ * 23, icon.width, icon.height), icon);
				}
			}
			GUI.EndGroup();

			if (GUIX.Button(new Rect(rectSocialWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectSocialWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, this.guiSettings.GUIStyleButtonClose))
			{
				this.CloseSocialWindow();
			}
		}

		#endregion

		#region Helper Methods

		void LoadGUISkin(int skin)
		{
			this.textureInteractionWindow = (Texture2D)Resources.Load("gui/win_interaction_s" + skin);
			this.textureStoreWindow = (Texture2D)Resources.Load("gui/win_shop_s" + skin);
			this.textureLootWindow = (Texture2D)Resources.Load("gui/win_loot_s" + skin);
			this.textureCharacterInfo = (Texture2D)Resources.Load("gui/win_character_s" + skin);
			this.textureAbilitiesWindow = (Texture2D)Resources.Load("gui/win_ability_s" + skin);
			this.textureReputationWindow = (Texture2D)Resources.Load("gui/win_reputation_s" + skin);
			this.textureJournalWindow = (Texture2D)Resources.Load("gui/win_journal_s" + skin);
			this.textureOptionsWindow = (Texture2D)Resources.Load("gui/win_options_s" + skin);
			this.textureFriendsWindow = (Texture2D)Resources.Load("gui/win_friends_s" + HUD.SkidId);
		}

		void CalculateGUIRects()
		{
			#region Interaction

			this.closeButtonOffsetXMax = 11;
			this.closeButtonOffsetY = 10;
			this.closeButtonSize = 16;

			this.rectInteractionWindow = new Rect
				{
					x = 10,
					y = (Screen.height - textureInteractionWindow.height) / 2f,
					width = textureInteractionWindow.width,
					height = textureInteractionWindow.height
				};

			this.rectInteractionWindowTitle = new Rect
				{
					x = rectInteractionWindow.x + 12,
					y = rectInteractionWindow.y + 8,
					width = rectInteractionWindow.width - 24,
					height = 25
				};

			this.rectInteractionGroup = new Rect
				{
					x = rectInteractionWindow.x + 28,
					y = rectInteractionWindow.y + 55,
					width = rectInteractionWindow.width - 45,
					height = rectInteractionWindow.height - 125
				};

			this.rectInteractionContainer = new Rect
				{
					x = 0,
					y = 0,
					width = rectInteractionGroup.width,
					height = rectInteractionGroup.height
				};

			this.rectFarewellButton = new Rect
				{
					x = rectInteractionWindow.x + (rectInteractionWindow.width / 2) - 50,
					y = rectInteractionWindow.yMax - 37,
					width = 100,
					height = 26
				};

			#endregion

			#region Merchant

			this.rectStoreWindow = new Rect
				{
					x = 20,
					y = (Screen.height - textureStoreWindow.height) / 2f,
					width = textureStoreWindow.width,
					height = textureStoreWindow.height
				};

			this.rectItem = new Rect
				{
					x = 0,
					y = 0,
					width = GameSettings.ITEM_ICON_SIZE,
					height = GameSettings.ITEM_ICON_SIZE
				};

			#endregion

			#region Loot

			this.rectLootWindow = new Rect
				{
					x = (Screen.width - textureLootWindow.width) / 2f,
					y = (Screen.height - textureLootWindow.height) / 2 - 50,
					width = textureLootWindow.width,
					height = textureLootWindow.height
				};

			this.rectLootGroup = new Rect
				{
					x = rectLootWindow.x + 9,
					y = rectLootWindow.y + 42,
					width = 193,
					height = 166
				};

			this.rectLootTakeAllButton = new Rect
				{
					x = rectLootWindow.x + (rectLootWindow.width - 100) / 2,
					y = rectLootWindow.yMax - 37,
					width = 100,
					height = 26
				};

			this.rectLootContainer = new Rect(0, 0, rectLootGroup.width, rectLootGroup.height);

			#endregion

			#region Character
			
			this.rectCharacterWindow = new Rect
				{
					x = (Screen.width - textureCharacterInfo.width) / 2f,
					y = (Screen.height - textureCharacterInfo.height) / 2f,
					width = textureCharacterInfo.width,
					height = textureCharacterInfo.height
				};

			this.rectCharacterStat = new Rect
				{
					x = rectCharacterWindow.x + 155,
					y = rectCharacterWindow.y + 359,
					width = 45,
					height = 16
				};

			this.rectCharacterTab = new Rect(rectCharacterWindow.x + 11, rectCharacterWindow.y + 486, 115, 31);
			this.rectAbilitiesTab = new Rect(rectCharacterTab.xMax + 2, rectCharacterTab.y, rectCharacterTab.width, rectCharacterTab.height);
			this.rectReputationTab = new Rect(rectAbilitiesTab.xMax + 2, rectCharacterTab.y, rectCharacterTab.width, rectCharacterTab.height);

			this.characterStatPosX = (int) rectCharacterStat.x;
			this.characterStatPosY = (int) rectCharacterStat.y;

			this.characterStatOffsetY = 14.5f;
			this.characterStatOffsetX = 172;

			this.rectItemIcon = new Rect
				{
					x = 0,
					y = 0,
					width = GameSettings.ITEM_ICON_SIZE,
					height = GameSettings.ITEM_ICON_SIZE
				};

			this.spellIconOffsetX = (int) rectCharacterWindow.x + 21;
			this.spellIconOffsetY = (int) rectCharacterWindow.y + 60;

			#endregion

			#region Journal

			this.rectJournalWindow = new Rect
				{
					x = (Screen.width - textureJournalWindow.width) / 2f,
					y = (Screen.height - textureJournalWindow.height) / 2f,
					width = textureJournalWindow.width,
					height = textureJournalWindow.height
				};

			this.rectJournalListGroup = new Rect
				{
					x = rectJournalWindow.x + 27,
					y = rectJournalWindow.y + 57,
					width = 285,
					height = 385
				};

			this.rectJournalListContainer = new Rect
				{
					x = 0,
					y = 0,
					width = rectJournalListGroup.width,
					height = rectJournalListGroup.height
				};

			this.rectJournalContentGroup = new Rect
				{
					x = rectJournalWindow.x + 370,
					y = rectJournalWindow.y + 55,
					width = rectInteractionContainer.width,
					height = rectInteractionContainer.height
				};

			this.rectJournalContentContainer = new Rect
				{
					x = 0,
					y = 0,
					width = rectJournalContentGroup.width,
					height = rectJournalContentGroup.height
				};

			#endregion

			#region Options

			this.rectOptionsWindow = new Rect
				{
					x = (Screen.width - textureOptionsWindow.width) / 2f,
					y = (Screen.height - textureOptionsWindow.height) / 2f,
					width = textureOptionsWindow.width,
					height = textureOptionsWindow.height
				};

			#endregion

			#region Social

			this.rectSocialWindow = new Rect
				{
					x = (Screen.width - textureFriendsWindow.width) / 2f,
					y = (Screen.height - textureFriendsWindow.height) / 2f,
					width = textureFriendsWindow.width,
					height = textureFriendsWindow.height
				};

			this.rectSocialContent = new Rect
				{
					x = rectSocialWindow.x + 33,
					y = rectSocialWindow.y + 66,
					width = 315,
					height = 385
				};

			#endregion
		}

		static Texture2D GetMenuItemIcon(MenuIconType iconType)
		{
			switch (iconType)
			{
				case MenuIconType.QuestActive:
					return GameResources.Instance.IconMenuQuestActive;

				case MenuIconType.QuestInProgress:
					return GameResources.Instance.IconMenuQuestInProgress;

				case MenuIconType.QuestTurnIn:
					return GameResources.Instance.IconMenuQuestComplete;

				case MenuIconType.Conversation:
					return GameResources.Instance.IconMenuConversation;
			}

			return null;
		}

		static Texture2D GetSocialStatusIcon(SocialStatus socialStatus)
		{
			switch (socialStatus)
			{
				case SocialStatus.Online:
					return GameResources.Instance.IconSocialStatusOnline;

				case SocialStatus.Away:
					return GameResources.Instance.IconSocialStatusAway;

				case SocialStatus.Offline:
					return GameResources.Instance.IconSocialStatusOffline;
			}

			return null;
		}

		void World_OnPlayerAdded(Player pActor)
		{
			this.player = pActor;
		}

		void ClearILs()
		{
			this.ILRects.Clear();
			this.ILStrings.Clear();
			this.ILContents.Clear();
		}

		#endregion
	};
}